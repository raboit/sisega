<?php

class MallaController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->layout='//layouts/column1';
//		$dataProvider = new CArrayDataProvider(Asignatura::model()->findAll(),
//                            array('pagination'=>false)
//                        );
                $records = Asignatura::model()->findAll();
//                $asignaturas = array_column($records, null, 'numero');
//                print_r(GxHtml::listDataEx($records, 'numero'));
                $asignaturas = array();
                if(count($records) > 0){
                    foreach($records as $record)
                        $asignaturas[$record->numero] = (object)$record->attributes;
                }       
		$this->render('index', array(
			'asignaturas' => $asignaturas,
		));
	}
        

}