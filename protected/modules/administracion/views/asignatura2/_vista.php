<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('ver', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
        <?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('semestre')); ?>:
        <?php echo GxHtml::encode($data->semestre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('programa')); ?>:
        <?php echo GxHtml::encode($data->programa); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('codigo')); ?>:
        <?php echo GxHtml::encode($data->codigo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero')); ?>:
        <?php echo GxHtml::encode($data->numero); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('catedra')); ?>:
        <?php echo GxHtml::encode($data->catedra); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('taller')); ?>:
        <?php echo GxHtml::encode($data->taller); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('laboratorio')); ?>:
        <?php echo GxHtml::encode($data->laboratorio); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_formacion')); ?>:
        <?php echo GxHtml::encode($data->tipo_formacion); ?>
	<br />
	*/ ?>

</div>
</a>