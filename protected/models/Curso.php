<?php

Yii::import('application.models._base.BaseCurso');

class Curso extends BaseCurso
{
        public $repColumnsSeparator = ' - ';
    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
//        public static function representingColumn() {
//                return array('nombre', 'anio', 'semestre');
//        }
  
        public static function representingColumn() {
		return 'NombreRepresentativo';
	}
        
        public function rules() {
            return array_merge(parent::rules(), array(
                        array('estado', 'required'),
                    ));
        }        
        
        public function getNombre(){
                return Asignatura::model()->findByPk($this->asignatura_id)->nombre;
        }
        
        public function getNombreRepresentativo(){
                $nombre = Asignatura::model()->findByPk($this->asignatura_id)->nombre;
                $nombre .= ' ('.$this->semestre.' - '.$this->anio.')';
                return $nombre;
        }
        
        public static function cursos_alumno($alumno_id, $activo){
            
            $criteria=new CDbCriteria(array(
                'with' => array('alumnos'),
                'condition'=>'alumno_id = :alumno_id',
                'order'=>'t.id DESC',
                'limit'=>500,
                'params'=> array(':alumno_id' => $alumno_id),
             ));
            
            $dataProvider=new CActiveDataProvider('Curso',array('criteria'=>$criteria,));	

            return ($dataProvider);
        }
       
        public static function estados(){
                return array('Activo'=>'Activo', 'Inactivo'=>'Inactivo');
        }

           public function scopes()
        {
            return array(
                'activos'=>array(
                    'condition'=>"estado='ACTIVO'",
                ),
            );
        }
        
        public function cursos2($alumno_id,$anio)

                {
                    $criteria = new CDbCriteria;
                    $criteria->together = true;
                    $criteria->with = array('alumnos');
                    $criteria->compare('alumnos.id', $alumno_id);
                    $criteria->condition='alumnos_alumnos.alumno_id=:alumno_id  AND alumnos_alumnos.anio=:anio';
                     $criteria->params=array(':alumno_id'=>$alumno_id,':anio'=>$anio);
                    $this->getDbCriteria()->mergeWith($criteria);
                    return $this;
                }  
                
        public function getPromedio($alumno_id=NULL,$curso_id=NULL){
            CursoTieneAlumno::model()->findAllByAttributes('promedio', array('alumno_id=:alumno_id AND curso_id=:curso_id'), array(':alumno_id'=>$alumno_id,':curso_id'=>$curso_id));
            return 45;
        }
}