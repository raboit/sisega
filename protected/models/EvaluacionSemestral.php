<?php

Yii::import('application.models._base.BaseEvaluacionSemestral');

class EvaluacionSemestral extends BaseEvaluacionSemestral
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'alumno_id' => null,
			'semestre' => Yii::t('app', 'Semestre'),
			'anio' => Yii::t('app', 'Anio'),
			'estado' => Yii::t('app', 'Aprobado'),
			'observacion' => Yii::t('app', 'Observacion'),
			'oportunidad' => Yii::t('app', 'Oportunidad'),
			'semestre_cursado' => Yii::t('app', 'Semestre Cursado'),
			'promedio' => Yii::t('app', 'Promedio'),
			'alumno' => null,
		);
	}
}