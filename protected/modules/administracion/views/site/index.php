<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	//$this->module->id,
        Yii::t('app', $this->module->id)
);
?>
<?php $this->widget('bootstrap.widgets.TbHeroUnit', array(
    'heading' => 'Módulo Administración',
    'content' => TbHtml::lead('<p>Bienvenido al módulo de Administración</p>') .
    TbHtml::em('Este módulo no posee lógica de negocio, por lo tanto use con precaución.', array('color' => TbHtml::TEXT_COLOR_ERROR)),
)); ?>