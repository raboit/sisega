<?php
$this->breadcrumbs = array(
       Yii::app()->user->profesor => Yii::app()->createUrl('academico/panel'),
       GxHtml::encode(Curso::label(2))=> Yii::app()->createUrl('academico/misCursos'),
       GxHtml::encode(Curso::model()->findByPk($model->curso_id)) => Yii::app()->createUrl('academico/verCurso', array('id'=>$model->curso_id)),
       //$model->label(2)=> Yii::app()->createUrl('panel/profesor'),
       Yii::t('app', 'Create') . ' ' . $model->label(1),
);

$this->menu = array(
//        array('label'=>Yii::t('app', 'Operations')),
//        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('listar'), 'icon'=>'list'),
//        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()), null); ?>

<div class="container inline"><?php
        $actividades=new Actividad;
        $total=0;
        foreach($model_planificacion_semestral->actividads as $i=>$actividad):?>
            <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, '<strong>'.$actividad->fecha_inicio.' - '.$actividad->fecha_termino.'</strong> <br>'.$actividad->detalle,array('class'=>'span2')); ?>
            <?php $total++; ?>
        <?php endforeach;?>
    <?php
$this->widget('bootstrap.widgets.TbAlert', array());
?>
</div>  
    
    <?php
$this->renderPartial('planActividad/_formPlanActividad', array(
		'model' => $model,
                'model_planificacion_semestral' => $model_planificacion_semestral,
		'buttons' => 'create'));
?>