<?php

$this->breadcrumbs = array(
        Yii::t('app', $this->module->id) => Yii::app()->createUrl($this->module->baseUrl),  
	$model->label(2) => array('listar'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()), TbHtml::labelTb('Admin')); ?>
<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'calificaciones-grid',
        'dataProvider' => new CArrayDataProvider($cursos_alumno,array()),
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
        'columns' => array(
                array(
                        'name' => Asignatura::label(),
                        'type' => 'raw',
                        'value' => 'GxHtml::valueEx($data->asignatura)',
                        ),
                array(
                        'name' => Profesor::label(),
                        'type' => 'raw',
                        'value' => '$data->profesor',
                        ),
            array(
                        'name' => 'promedio',
                        'type' => 'raw',
                        'value' => '$data->getPromedio()',
                        ),
             
        ),
));
?>


<?php

$this->renderPartial('_vistaListarAlumnos', array(
                'model' => $model,
                'buttons' => 'create'));
// $this->renderPartial('_formulario', array(
// 		'model' => $model,
// 		'buttons' => 'create'));
?>
