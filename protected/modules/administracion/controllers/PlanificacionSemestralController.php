<?php /*

class PlanificacionSemestralController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('PlanificacionSemestral');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'PlanificacionSemestral'),
		));
	}

	public function actionCrear() {
		$model = new PlanificacionSemestral;

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'PlanificacionSemestral');

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);
                        
			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PlanificacionSemestral')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new PlanificacionSemestral('search');
		$model->unsetAttributes();

		if (isset($_GET['PlanificacionSemestral'])){
			$model->setAttributes($_GET['PlanificacionSemestral']);
                }

                $session['PlanificacionSemestral_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['PlanificacionSemestral_model_search']))
               {
                $model = $session['PlanificacionSemestral_model_search'];
                $model = PlanificacionSemestral::model()->findAll($model->search()->criteria);
               }
               else
                 $model = PlanificacionSemestral::model()->findAll();
             $this->toExcel($model, array('id', 'calendarioDocente', 'fecha_creacion', 'fecha_proposicion', 'fecha_respuesta', 'estado', 'fecha_inicio', 'fecha_termino', 'user'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['PlanificacionSemestral_model_search']))
               {
                $model = $session['PlanificacionSemestral_model_search'];
                $model = PlanificacionSemestral::model()->findAll($model->search()->criteria);
               }
               else
                 $model = PlanificacionSemestral::model()->findAll();
             $this->toExcel($model, array('id', 'calendarioDocente', 'fecha_creacion', 'fecha_proposicion', 'fecha_respuesta', 'estado', 'fecha_inicio', 'fecha_termino', 'user'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

}
 * 
 */


class PlanificacionSemestralController extends GxController {

        
        public function actionProponerPlanificacion($id){
            $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
            $model_planificacion->estado = "PROPUESTA";
            $model_planificacion->fecha_proposicion = date("d/m/Y");
            
            $model_planificacion->save(FALSE);
            $this->redirect(array('verPlanificacion', 'id' => $model_planificacion->id));
            
        }
    
        public function actionVerPlanificaciones(){
           
                $model_planificacion = new PlanificacionSemestral('search');
                $model_planificacion->unsetAttributes();
                $tipo = "Planificacion";
                if (isset($_GET['PlanificacionSemestral'])){
			$model_planificacion->setAttributes($_GET['PlanificacionSemestral']);
                }
                $this->render('planificacionSemestral/ver_planificaciones', array(
			'model_planificacion' => $model_planificacion,
                        'tipo' => $tipo,
		));
        }
    
        public function actionCrearPlanificacion(){
            $rango_fecha = CalendarioDocente::obtenerRangoFechas();
            $model_planificacion = new PlanificacionSemestral;
            $model_planificacion->fecha_creacion = date("d/m/Y");
            $model_planificacion->calendario_docente_id = $rango_fecha[2];
            
            $this->performAjaxValidation($model_planificacion, 'planificacion-semestral-form');

            if (isset($_POST['PlanificacionSemestral'])) {
                $model_planificacion->setAttributes($_POST['PlanificacionSemestral']);
                $model_planificacion->estado = 'NUEVA'; 
                $rango_semestre = CalendarioDocente::obtenerRangoSemestre($model_planificacion->semestre);
                $model_planificacion->fecha_inicio=$rango_semestre[0];
                $model_planificacion->fecha_termino=$rango_semestre[1];
                if ($this->validarFechasPlanificacionSemestral($model_planificacion) && $model_planificacion->save()) {
                        $model_planificacion->generarActividades();    
                    if (Yii::app()->getRequest()->getIsAjaxRequest())
                                Yii::app()->end();
                        else
                                $this->redirect(array('verPlanificacion', 'id' => $model_planificacion->id));
                }
            }
            
		$this->render('planificacionSemestral/crear_planificacion_semestral',array( 
                            'model_planificacion' => $model_planificacion,
                            'rango_fecha' => $rango_fecha,
                        ));
         
       }
        
        public function actionEditarPlanificacion($id){
            
            $rango_fecha = CalendarioDocente::obtenerRangoFechas();
            $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');

            $this->performAjaxValidation($model_planificacion, 'planificacion-semestral-form');

            if (isset($_POST['PlanificacionSemestral'])){
                    $model_planificacion->setAttributes($_POST['PlanificacionSemestral']);
                    if ($this->validarFechasPlanificacionSemestral($model_planificacion) && $model_planificacion->save()) {
                            $this->redirect(array('verPlanificacion', 'id' => $model_planificacion->id));
                    }
            }
            $this->render('planificacionSemestral/editar_planificacion_semestral', array(
                            'model_planificacion' => $model_planificacion,
                            'rango_fecha' => $rango_fecha
            ));
        }
        
        public function actionVerPlanificacion($id){
            $model_actividad = new Actividad();
            $model_actividad_planificacion = $model_actividad->misActividades($id);
            $model = $this->loadModel($id, 'PlanificacionSemestral');
            $propuesto = TRUE;
            
            if($model->estado == "PROPUESTA")
                $propuesto = FALSE;
                
            $this->render('planificacionSemestral/ver_planificacion_semestral', array(
                    'model' => $model, 
                    'model_actividad_planificacion' => $model_actividad_planificacion,
                    'propuesto' => $propuesto,
            ));
	}
    
        public function actionVerActividades(){
            $model_actividad = new Actividad('search');
            $model_actividad->unsetAttributes();
            $tipo = "Actividad";

		if (isset($_GET['Actividad'])){
			$model_actividad->setAttributes($_GET['Actividad']);
                }
            $this->render('actividad/ver_actividades', array(
                    'model_actividad' => $model_actividad,
                    'tipo' => $tipo,
            ));
        }
        
        public function actionCrearActividad($id){
            $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
            $model_actividad = new Actividad;
            $model_actividad->planificacion_semestral_id = $model_planificacion->id;
            $rango_fechas = CalendarioDocente::obtenerRangoFechas();
            $this->performAjaxValidation($model_actividad, 'actividad-form');
            if (isset($_POST['Actividad'])) {
                $model_actividad->setAttributes($_POST['Actividad']);
                if ($this->validarFechas($model_actividad, $model_planificacion) && $model_actividad->save()){
                        if (Yii::app()->getRequest()->getIsAjaxRequest())
                                Yii::app()->end();
                        else
                                $this->redirect(array('planificacion/verPlanificacion', 'id' => $model_planificacion->id));
                }
            }
            $this->render('actividad/crear_actividad', array( 
                'model_actividad' => $model_actividad,
                'model_planificacion' => $model_planificacion,
                'rango_fechas'=>$rango_fechas,
                ));
        }
        
        public function actionEditarActividad($id){
            $model_actividad = $this->loadModel($id, 'Actividad');
            $model_planificacion = $this->loadModel($model_actividad->planificacion_semestral_id, 'PlanificacionSemestral');
            $this->performAjaxValidation($model_actividad, 'actividad-form');
            if (isset($_POST['Actividad'])) {
                    $model_actividad->setAttributes($_POST['Actividad']);
                    if ($this->validarFechas($model_actividad, $model_planificacion) && $model_actividad->save()) {
                            $this->redirect(array('planificacion/verPlanificacion', 'id' => $model_planificacion->id));
                    }
            }

            $this->render('actividad/editar_actividad', array(
                            'model_actividad' => $model_actividad,
                            'model_planificacion' => $model_planificacion,
                            ));
        }
        
        public function actionVerActividad($id){
            $this->render('actividad/ver_actividad', array(
			'model' => $this->loadModel($id, 'Actividad'), 
                ));
        }
        
        public function filters() {
            return array('rights');
        }
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        private function validarFechas($model, $model_planificacion_semestral){
        $error = false;
        if(!Yii::app()->utilidad->compararRangoFechas($model->fecha_inicio, $model_planificacion_semestral->fecha_inicio, '>=')
                or !Yii::app()->utilidad->compararRangoFechas($model->fecha_inicio, $model_planificacion_semestral->fecha_termino, '<=')){
                $model->addError('fecha_inicio', Yii::t('app', 'La fecha de inicio no concuerda con la fecha de inicio de la planificación semestral'));
                $error = true;
        }
        if(!Yii::app()->utilidad->compararRangoFechas($model->fecha_inicio, $model->fecha_termino, '<=')){
                $model->addError('fecha_termino', Yii::t('app', 'La fecha de termino es inferior a la fecha de inicio'));
                $error = true;
        }else{                      
        if(!Yii::app()->utilidad->compararRangoFechas($model->fecha_termino, $model_planificacion_semestral->fecha_termino, '<=')
                or !Yii::app()->utilidad->compararRangoFechas($model->fecha_termino, $model_planificacion_semestral->fecha_inicio, '>=')){
                $model->addError('fecha_termino', Yii::t('app', 'La fecha de termino no concuerda con la fecha de termino de la planificación semestral'));
                $error = true;                                
        }}
        return !$error;
        }
        
        private function validarFechasPlanificacionSemestral($model){
        $error = false;
            if(!Yii::app()->utilidad->compararRangoFechas($model->fecha_inicio, $model->fecha_termino, '!=')){
                    $model->addError('fecha_termino', Yii::t('app', 'Las fechas no pueden ser iguales'));
                    $error = true;
            }
            if(!Yii::app()->utilidad->compararRangoFechas($model->fecha_inicio, $model->fecha_termino, '<=')){
                    $model->addError('fecha_termino', Yii::t('app', 'La fecha de termino es inferior a la fecha de inicio'));
                    $error = true;
            }
            
            
            return !$error;
        }
        
        public function actionGenerarExcel($tipo)
	{	   
             
             $session=new CHttpSession;
             $session->open();
             if($tipo == "Planificacion"){
               if(isset($session['PlanificacionSemestral_model_search'])){
                   $model = $session['PlanificacionSemestral_model_search'];
                   $model = PlanificacionSemestral::model()->findAll($model->search()->criteria);
               }
               else
                 $model = PlanificacionSemestral::model()->findAll();
               $this->toExcel($model, array('id', 'calendarioDocente', 'fecha_creacion', 'fecha_proposicion', 'fecha_respuesta', 'estado', 'fecha_inicio', 'fecha_termino'), date('Y-m-d-H-i-s'), array(), 'Excel5');
                
             }else if($tipo == "Actividad"){
               if(isset($session['Actividad_model_search'])){
                   $model = $session['Actividad_model_search'];
                   $model = Actividad::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Actividad::model()->findAll();
               $this->toExcel($model, array('id', 'planificacionSemestral', 'fecha_inicio', 'fecha_termino', 'detalle'), date('Y-m-d-H-i-s'), array(), 'Excel5'); 
                 
             }
             
	}
        
        public function actionVerPlanificacionPropuesta(){
        $model_planificacion_semestral = PlanificacionSemestral::cargarPlanificacion();   
        $model_actividad = new Actividad($model_planificacion_semestral->id);
        $model_actividades_planificacion = $model_actividad->misActividades($model_planificacion_semestral->id);
        $estado = TRUE;
        
        if($model_planificacion_semestral->estado == "VIGENTE" OR $model_planificacion_semestral->estado == "RECHAZADA"){
             $estado = FALSE;
        }
        $this->render('ver_planificacion_propuesta', array(
                    'model_planificacion_semestral' => $model_planificacion_semestral, 
                    'model_actividades_planificacion' => $model_actividades_planificacion,
                    'estado' => $estado,
        ));
        
    }
    
    public function actionAceptarPlanificacion($id){
        $user_id = Yii::app()->user->id;
        
        $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
        $model_planificacion->estado = "VIGENTE";
        $model_planificacion->fecha_respuesta = date("d/m/Y");
        $model_planificacion->user_id = $user_id;
        
        $model_planificacion->save(TRUE);
        $this->redirect(array('verPlanificacion', 'id'=>$id));
        
    }
    
    public function actionRechazarPlanificacion($id){
        $user_id = Yii::app()->user->id;
        
        $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
        $model_planificacion->estado = "RECHAZADA";
        $model_planificacion->fecha_respuesta = date("d/m/Y");
        $model_planificacion->user_id = $user_id;

        $model_planificacion->save(FALSE);
        $this->redirect(array('verPlanificacion', 'id'=>$id));
    }
        
        
}