<?php

Yii::import('application.models._base.BaseProfesor');

class Profesor extends BaseProfesor
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function rules() {
            return array_merge(parent::rules(), array(
                        array('nombre', 'required'),
                    ));
        }
}