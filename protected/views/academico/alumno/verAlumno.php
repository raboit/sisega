<?php

$this->breadcrumbs = array(
       Yii::app()->user->profesor => Yii::app()->createUrl('academico/panel'),
       GxHtml::encode(Curso::label(2))=> Yii::app()->createUrl('academico/misCursos'),
       GxHtml::valueEx($model_curso)=>Yii::app()->createUrl('academico/verCurso', array('id'=>$model_curso->id)),
       GxHtml::valueEx($model),
);

$this->menu=array(
//        array('label'=>Yii::t('app', 'Operations')),
//        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
//        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
//        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
//        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
//        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'nombre',
		'run',
		'direccion',
//		array(
//			'name' => 'user',
//			'type' => 'raw',
//			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('user/ver', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
//			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('calificacions')); ?></h2>
<?php $labelCalificacion = Calificacion::model()->attributeLabels(); ?>
<?php // $labelEvaluacion = Evaluacion::model()->attributeLabels(); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'calificacion-grid',
	'dataProvider' => new CArrayDataProvider($model->calificacions),
        'type'=>'striped bordered condensed',
        'template'=>"{items}{pager}{summary}",
	'columns' => array(
		//'id',
		array(
                        'name'=> $model->getRelationLabel('evaluacion'),
                        'value'=>'$data->evaluacion->nombre',
                ),
		array(
                        'name'=> $labelCalificacion['nota'],
                        'value'=>'$data->nota',
                ),
        array(
              'class'=>'bootstrap.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->controller->createUrl("verCalificacion", array("id"=>$data->evaluacion->id))',
                'updateButtonUrl'=>'Yii::app()->controller->createUrl("editarCalificacion", array("id"=>$data->evaluacion->id))',
                'deleteButtonUrl'=>'Yii::app()->controller->createUrl("borrarCalificacion", array("id"=>$data->evaluacion->id))',
//                'deleteConfirmation'=>Yii::t('app','¿Seguro que desea borrar este elemento?, esto provocará que se eliminen todas las calificaciones asociadas.'),
          ),                
	),
)); ?>