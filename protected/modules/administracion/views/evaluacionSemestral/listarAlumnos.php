<script type="text/javascript">
  
    
    function crearEvaluacion(url)
    {
        <?php echo CHtml::ajax(array(
                'url'=>'js:url',
                'data'=> "js:$(this).serialize()",
                'type'=>'post',
                'dataType'=>'json',
                'success'=>"function(data)
                {
                    if (data.status == 'failure')
                    {
                        $('#evaluacion_contenido').html(data.div);
                        $('#evaluacion_contenido').show();
                        $('#evaluacion_contenido form').submit(crearEvaluacion(js.url));
                    }
                    else
                    {
                        $('#evaluacion_contenido').html(data.mensaje);
                       
                        setTimeout(\"$('#myModal').modal('hide');\",500);

                     }              
                }",
                ))
        ?>;
        return false;  
    } 
</script>


<?php

$this->breadcrumbs = array(
        Yii::t('app', $this->module->id) => Yii::app()->createUrl($this->module->baseUrl),  
	EvaluacionSemestral::label(2),
);

$this->menu = array(
        
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
        array('label'=>Yii::t('app', 'Up'), 'url'=>'javascript:GoUp()', 'icon'=>'arrow-up', 'id'=>'button-up'),
);
?>
<?php echo TbHtml::pageHeader(GxHtml::encode(EvaluacionSemestral::label(2)), TbHtml::labelTb('Admin')); ?>
<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,null,'post',array('id' => 'detalle-producto-form',)); ?>
    <fieldset>
        <legend>Ingresar Producto</legend>
        <?php echo TbHtml::label('Buscar', 'texto'); ?>
        <?php echo TbHtml::textField('Año', date("Y"),array('value'=>date("Y"), 'id' =>'anio', 'maxlength'=>36, 'placeholder'=>'2013')); ?>
        <?php echo TbHtml::dropDownList('Semestre', Null ,array(1 =>'I Semestre',2 =>'II Semestre'),array('id'=>'semestre')); ?><br>
        <?php echo TbHtml::submitButton('Buscar',array('icon'=>'search white', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        
    </fieldset>
<?php echo TbHtml::endForm(); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'evaluacion-semestral-grid',
	'dataProvider' =>$dataProvider,	
	'columns' => array(
		'id',
		
		'nombre',
		
		'run',
		'direccion',
		'anio_ingreso',
		
            array(
                'class'=>'CButtonColumn',
                'template' => '{postview} ',
                'buttons' => array(
                    'postview' => array(
                        'label' => 'Evaluar', 
                       'url' => '$this->grid->controller->createUrl("evaluacionSemestral/crearEvaluacion", array("id"=>$data->id, "semestre"=>'.$semestre.'))', 
                        
                    )

                    )
            )
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'myModal',
    'header' => 'Evaluarcion semestreal',
    'content' => '<div id="evaluacion_contenido" ></div>',
    
)); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/scroll.js'); ?>