<?php

class EvaluacionPlanificacionController extends GxController {

    
    public function actionVerPlanificacionPropuesta(){
        $model_planificacion_semestral = PlanificacionSemestral::cargarPlanificacion();   
        $model_actividad = new Actividad($model_planificacion_semestral->id);
        $model_actividades_planificacion = $model_actividad->misActividades($model_planificacion_semestral->id);
        $estado = TRUE;
        
        if($model_planificacion_semestral->estado == "VIGENTE" OR $model_planificacion_semestral->estado == "RECHAZADA"){
             $estado = FALSE;
        }
        $this->render('ver_planificacion_propuesta', array(
                    'model_planificacion_semestral' => $model_planificacion_semestral, 
                    'model_actividades_planificacion' => $model_actividades_planificacion,
                    'estado' => $estado,
        ));
        
    }
    
    public function actionAceptarPlanificacion($id){
        $user_id = Yii::app()->user->id;
        
        $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
        $model_planificacion->estado = "VIGENTE";
        $model_planificacion->fecha_respuesta = date("d/m/Y");
        $model_planificacion->user_id = $user_id;
        
        $model_planificacion->save(TRUE);
        $this->redirect(array('verPlanificacionPropuesta'));
        
    }
    
    public function actionRechazarPlanificacion($id){
        $user_id = Yii::app()->user->id;
        
        $model_planificacion = $this->loadModel($id, 'PlanificacionSemestral');
        $model_planificacion->estado = "RECHAZADA";
        $model_planificacion->fecha_respuesta = date("d/m/Y");
        $model_planificacion->user_id = $user_id;

        $model_planificacion->save(FALSE);
        $this->redirect(array('verPlanificacionPropuesta'));
    }
}
